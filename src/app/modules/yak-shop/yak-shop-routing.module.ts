import { NgModule } from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {YAK_SHOP_ROUTE_FRAGMENTS} from './commons/constants/rotues'
import {HomeComponent} from './pages/home/home.component'
import {HerdComponent} from './pages/herd/herd.component'
import {YakShopComponent} from './yak-shop.component'
import {AuthGuard} from './commons/guards/auth/auth.guard'
import {GuestGuard} from './commons/guards/guest/guest.guard'
import {OrderComponent} from './pages/order/order.component'
import {ProductsComponent} from './pages/products/products.component'
import {NotFoundComponent} from './pages/not-found/not-found.component'

const YAK_SHOP_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: YAK_SHOP_ROUTE_FRAGMENTS.root
  },
  {
    path: YAK_SHOP_ROUTE_FRAGMENTS.root,
    component: YakShopComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: YAK_SHOP_ROUTE_FRAGMENTS.home
      },
      {
        path: YAK_SHOP_ROUTE_FRAGMENTS.home,
        component: HomeComponent,
        canActivate: [GuestGuard]
      },
      {
        path: YAK_SHOP_ROUTE_FRAGMENTS.herd,
        component: HerdComponent,
        canActivate: [AuthGuard]
      },
      {
        path: YAK_SHOP_ROUTE_FRAGMENTS.products,
        component: ProductsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: YAK_SHOP_ROUTE_FRAGMENTS.order,
        component: OrderComponent,
        canActivate: [AuthGuard]
      },

    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(YAK_SHOP_ROUTES)
  ],
  exports: [
    RouterModule
  ]
})
export class YakShopRoutingModule { }
