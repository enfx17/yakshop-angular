import { Component, OnInit } from '@angular/core'
import {IMenuItem} from '../../commons/models/MenuItem.model'
import {IProduct, ShoppingCartProduct} from '../../commons/models/Product.model'
import {UserService} from '../../commons/services/user/user.service'
import {ShoppingCartService} from '../../commons/services/shopping-cart/shopping-cart.service'
import {MAIN_MENU} from '../../commons/constants/main-menu'
import {YAK_SHOP_ROUTE_FRAGMENTS} from '../../commons/constants/rotues'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  addedToCart: ShoppingCartProduct
  cartRoute = `/${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.order}`

  products: IProduct[] = [
    { id: 'milk', name: 'Milk', icon: '/assets/milk.svg'},
    { id: 'skins', name: 'Wool', icon: '/assets/wool.svg'},
  ]

  constructor(
    private userService: UserService,
    private shoppingCart: ShoppingCartService
  ) { }

  ngOnInit(): void {
  }

  addToShoppingCart(product: ShoppingCartProduct): void {
    this.addedToCart = product
    this.shoppingCart.addProduct(product)
  }

}
