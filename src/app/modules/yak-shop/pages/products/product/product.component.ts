import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core'
import {IProduct, ShoppingCartProduct} from '../../../commons/models/Product.model'
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {ShoppingCartService} from '../../../commons/services/shopping-cart/shopping-cart.service'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: IProduct
  @Output() addToCart = new EventEmitter<ShoppingCartProduct>()

  quantityForm: FormGroup

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.quantityForm = this.buildQuantityForm()
  }

  buildQuantityForm(): FormGroup {
    return this.formBuilder.group({
      quantity: ['', [Validators.required, Validators.min(0)]]
    })
  }

  submit(): void {
    if (this.quantityForm.valid) {
      this.addToCart.emit(new ShoppingCartProduct(this.product, this.quantityForm.value.quantity))
      this.quantityForm.reset({ quantity: null })
    }
  }

}
