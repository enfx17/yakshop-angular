import { Component, OnInit } from '@angular/core'
import {ShoppingCartProduct} from '../../commons/models/Product.model'
import {UserService} from '../../commons/services/user/user.service'
import {ShoppingCartService} from '../../commons/services/shopping-cart/shopping-cart.service'
import {YakShopService} from '../../commons/services/yak-shop/yak-shop.service'
import {IOrder} from '../../commons/models/Order.model'

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  status: number
  username: string

  productsDelivered: any
  //
  // get productsDeliveredFormatted(): string[] {
  //   const productsDelivered = []
  //   for (const key of Object(this.productsDelivered).keys()) {
  //     productsDelivered.push(`${key.toLocaleUpperCase()} : ${this.productsDelivered[key]}`)
  //   }
  //   return productsDelivered
  // }

  constructor(
    private userService: UserService,
    private shoppingCart: ShoppingCartService,
    private yakShopService: YakShopService
  ) { }

  get cartProducts(): ShoppingCartProduct[] {
    return this.shoppingCart.cartProducts
  }

  ngOnInit(): void {
    this.username = this.userService.username()
  }

  removeProduct(id: string): void {
    this.shoppingCart.removeProduct(id)
  }

  order(): void {
    const orderProducts: any = {}
    this.cartProducts.forEach((product: ShoppingCartProduct) => {
      orderProducts[product.id] = product.quantity
    })

    const fullOrder: IOrder = {
      customer: this.userService.username(),
      order: orderProducts
    }

    this.yakShopService.placeOrder(fullOrder).subscribe(
      (response => {
        this.status = response.status
        this.productsDelivered = response.body
      }),
      (error => this.status = error.status)
    )
  }

}
