import { Component, OnInit } from '@angular/core'
import {YakShopComponent} from '../../yak-shop.component'
import {YakShopService} from '../../commons/services/yak-shop/yak-shop.service'
import {ActivatedRoute} from '@angular/router'
import {Herd} from '../../commons/models/Herd.model'
import {IMenuItem} from '../../commons/models/MenuItem.model'
import {UserService} from '../../commons/services/user/user.service'
import {MAIN_MENU} from '../../commons/constants/main-menu'

@Component({
  selector: 'app-herd',
  templateUrl: './herd.component.html',
  styleUrls: ['./herd.component.scss']
})
export class HerdComponent implements OnInit {
  menu: IMenuItem[] = MAIN_MENU

  herd: Herd

  constructor(
    private yakShopService: YakShopService,
    private activeRoute: ActivatedRoute,
    private userService: UserService
  ) {
    // this.activeRoute.params.subscribe(({ days }) => {
    //   this.initView(days)
    // })
    this.initView()
  }

  private initView(): void {
    this.yakShopService.getHerd().subscribe((herd: Herd) => {
      this.herd = herd
    })
  }

  ngOnInit(): void {
  }

}
