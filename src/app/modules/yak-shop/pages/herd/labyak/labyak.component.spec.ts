import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { LabyakComponent } from './labyak.component'

describe('LabyakComponent', () => {
  let component: LabyakComponent
  let fixture: ComponentFixture<LabyakComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabyakComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(LabyakComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
