import {Component, Input, OnInit} from '@angular/core'
import {Labyak} from '../../../commons/models/Labyak.model'

@Component({
  selector: 'app-labyak',
  templateUrl: './labyak.component.html',
  styleUrls: ['./labyak.component.scss']
})
export class LabyakComponent implements OnInit {
  @Input() labyak: Labyak

  shake = false

  constructor() { }

  ngOnInit(): void {
  }

  shakeLabyak(): void {
    this.shake = true
    setTimeout(() => {
      this.shake = false
    }, 1000)
  }

}
