import { Component, OnInit } from '@angular/core'
import {IMenuItem} from '../../commons/models/MenuItem.model'
import {YAK_SHOP_ROUTE_FRAGMENTS} from '../../commons/constants/rotues'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menu: IMenuItem[] = [
    { path: YAK_SHOP_ROUTE_FRAGMENTS.home, name: 'Home' },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
