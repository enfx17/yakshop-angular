import { Component, OnInit } from '@angular/core'
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {UserService} from '../../../commons/services/user/user.service'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  showLoginError = false
  loginForm: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) { }

  private buildLoginForm(): FormGroup {
    return this.formBuilder.group({
      username: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(5)]],
      password: ['', [Validators.required]],
      day: ['', [Validators.required, Validators.min(0)]]
    })
  }

  ngOnInit(): void {
    this.loginForm = this.buildLoginForm()
  }

  login(): void {
    console.log(this.loginForm)
    if (this.loginForm.valid) {
      this.showLoginError = !this.userService.login(this.loginForm.value)
    } else {
      this.loginForm.markAllAsTouched()
    }
  }

}
