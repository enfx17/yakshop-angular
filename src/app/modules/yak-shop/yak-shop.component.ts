import { Component, OnInit } from '@angular/core'
import {IMenuItem} from './commons/models/MenuItem.model'
import {YAK_SHOP_ROUTE_FRAGMENTS} from './commons/constants/rotues'
import {UserService} from './commons/services/user/user.service'
import {YakShopService} from './commons/services/yak-shop/yak-shop.service'
import {MAIN_MENU} from './commons/constants/main-menu'

@Component({
  selector: 'app-yak-shop',
  templateUrl: './yak-shop.component.html',
  styleUrls: ['./yak-shop.component.scss']
})
export class YakShopComponent implements OnInit {

  menu: IMenuItem[] = MAIN_MENU

  constructor(
    private userService: UserService,
    private yakShopService: YakShopService
  ) { }

  ngOnInit(): void {
  }

}
