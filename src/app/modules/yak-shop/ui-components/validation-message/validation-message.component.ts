import {Component, Input, OnInit} from '@angular/core'

@Component({
  selector: 'app-validation-message',
  templateUrl: './validation-message.component.html',
  styleUrls: ['./validation-message.component.scss']
})
export class ValidationMessageComponent implements OnInit {
  @Input() errors: any

  constructor() { }

  ngOnInit(): void {
  }

  get validationMessage(): string {
    if (this.errors.required) {
      return  'This input field is required.'
    }
    if (this.errors.minlength) {
      return `Min characters - ${this.errors.minlength.requiredLength}`
    }
    if (this.errors.maxlength) {
      return `Max characters - ${this.errors.maxlength.requiredLength}`
    }
    if (this.errors.maxlength) {
      return `Max characters - ${this.errors.maxlength.requiredLength}`
    }
    if (this.errors.min) {
      return `Min value - ${this.errors.min.min}`
    }
    if (this.errors.max) {
      return `Max value - ${this.errors.max.max}`
    }
  }

}
