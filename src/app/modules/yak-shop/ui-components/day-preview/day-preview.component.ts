import { Component, OnInit } from '@angular/core'
import {YakShopService} from '../../commons/services/yak-shop/yak-shop.service'

@Component({
  selector: 'app-day-preview',
  templateUrl: './day-preview.component.html',
  styleUrls: ['./day-preview.component.scss']
})
export class DayPreviewComponent implements OnInit {

  constructor(private yakShopService: YakShopService) { }

  ngOnInit(): void {
  }

  get day(): number {
    return this.yakShopService.day
  }

}
