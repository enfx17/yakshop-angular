import {Component, Input, OnInit} from '@angular/core'

import {IMenuItem} from '../../commons/models/MenuItem.model'


@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
  @Input() menu: IMenuItem[]

  constructor() { }

  ngOnInit(): void {
  }

}
