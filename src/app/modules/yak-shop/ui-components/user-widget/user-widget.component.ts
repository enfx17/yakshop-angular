import { Component, OnInit } from '@angular/core'
import {UserService} from '../../commons/services/user/user.service'

@Component({
  selector: 'app-user-widget',
  templateUrl: './user-widget.component.html',
  styleUrls: ['./user-widget.component.scss']
})
export class UserWidgetComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  get isLoggedIn(): boolean {
    return this.userService.isLoggedIn()
  }

  get username() {
    return this.userService.username()
  }

  logout() {
    this.userService.logout()
  }

}
