export const YAK_SHOP_ROUTE_FRAGMENTS = {
  root: 'yak-shop',
  home: 'home',
  herd: 'herd',
  products: 'products',
  order: 'order',
  notFound: '404'
}
