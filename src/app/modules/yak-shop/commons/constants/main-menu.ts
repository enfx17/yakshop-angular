import {YAK_SHOP_ROUTE_FRAGMENTS} from './rotues'
import {IMenuItem} from '../models/MenuItem.model'

export const MAIN_MENU: IMenuItem[] = [
  { path: `/${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.home}`, name: 'Home' },
  { path: `/${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.products}`, name: 'Products' },
  { path: `/${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.order}`, name: 'Order' },
  { path: `/${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.herd}`, name: 'Herd' },
]
