import {ILabyakResponse, Labyak} from './Labyak.model'

export interface IHerdResponse {
  herd: ILabyakResponse[]
}

export class Herd {
  herd: Labyak[]

  constructor(herdResponse: IHerdResponse) {
    this.herd = herdResponse.herd.map((labyakResponse: ILabyakResponse) => new Labyak(labyakResponse))
  }
}

