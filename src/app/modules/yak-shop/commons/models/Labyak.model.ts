export interface ILabyakResponse {
  name: string
  age: number
  'age-last-shaved'?: number
}

export class Labyak {
  name: string
  age: number
  ageLastShaved: number

  constructor(labyak: ILabyakResponse) {
    this.name = labyak.name
    this.age = labyak.age
    this.ageLastShaved = labyak['age-last-shaved']
  }
}
