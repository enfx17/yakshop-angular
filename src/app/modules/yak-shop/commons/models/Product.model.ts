export interface IProduct {
  id: string
  name: string
  icon: string
}

export class ShoppingCartProduct {

  id: string
  name: string
  quantity: number

  constructor(product: IProduct, quantity) {
    this.id = product.id
    this.name = product.name
    this.quantity = quantity
  }

}
