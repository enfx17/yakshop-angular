export interface ILoginRequest {
  username: string
  password: string
  day: number
}
