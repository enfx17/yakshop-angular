import { Injectable } from '@angular/core'
import {ILoginRequest} from '../../models/LoginRequest.model'
import {User} from '../../models/User.model'
import {Router} from '@angular/router'
import {YAK_SHOP_ROUTE_FRAGMENTS} from '../../constants/rotues'
import {YakShopService} from '../yak-shop/yak-shop.service'
import {ShoppingCartService} from '../shopping-cart/shopping-cart.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: User

  constructor(
    private router: Router,
    private yakShopService: YakShopService,
    private shoppingCart: ShoppingCartService
  ) { }

  login(loginRequest: ILoginRequest): boolean {
    if (loginRequest.username === loginRequest.password) {
      this.user = new User(loginRequest.username)
      this.yakShopService.day = loginRequest.day
      this.router.navigate([`${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.products}`])
      return true
    }
    return false
  }

  isLoggedIn(): boolean {
    return !!this.user
  }

  username(): string {
    return this.user && this.user.username
  }

  logout(): void {
    this.user = null
    this.shoppingCart.resetCart()
    this.router.navigate([`${YAK_SHOP_ROUTE_FRAGMENTS.root}/${YAK_SHOP_ROUTE_FRAGMENTS.home}`])
  }
}
