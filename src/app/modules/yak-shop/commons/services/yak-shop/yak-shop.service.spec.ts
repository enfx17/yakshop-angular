import { TestBed } from '@angular/core/testing'

import { YakShopService } from './yak-shop.service'

describe('YakShopService', () => {
  let service: YakShopService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(YakShopService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
