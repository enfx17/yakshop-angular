import { Injectable } from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {environment} from '../../../../../../environments/environment'
import {Herd, IHerdResponse} from '../../models/Herd.model'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'
import {ILabyakResponse} from '../../models/Labyak.model'
import {YAK_SHOP_ROUTE_FRAGMENTS} from '../../constants/rotues'
import {IMenuItem} from '../../models/MenuItem.model'
import {IOrder} from '../../models/Order.model'

@Injectable({
  providedIn: 'root'
})
export class YakShopService {

  private _day: number

  get day(): number {
    return this._day
  }

  set day(day: number) {
    this._day = day
  }

  static getYakShopAPIBaseUrl(): string {
    return `${environment.yakShopAPIHost}/yak-shop`
  }

  constructor(private httpClient: HttpClient) { }

  getHerd(): Observable<Herd> {
    const url = `${YakShopService.getYakShopAPIBaseUrl()}/herd/${this._day}`

    return this.httpClient.get<IHerdResponse>(url).pipe(map((herd: IHerdResponse) => {
      return new Herd(herd)
    }))
  }

  placeOrder(order: IOrder): Observable<any> {
    const url = `${YakShopService.getYakShopAPIBaseUrl()}/order/${this._day}`

    return this.httpClient.post(url, order,  {observe: 'response'})
  }
}
