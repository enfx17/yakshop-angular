import { Injectable } from '@angular/core'
import {IProduct, ShoppingCartProduct} from '../../models/Product.model'

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  private _products: ShoppingCartProduct[] = []

  addProduct(product: ShoppingCartProduct): void {
    const savedProduct = this._products.find((cartProduct: ShoppingCartProduct) => product.id === cartProduct.id)
    if (!savedProduct) {
      this._products.push(product)
    } else {
      savedProduct.quantity += product.quantity
    }
  }

  removeProduct(id: string): void {
    this._products = this._products.filter((product: ShoppingCartProduct) => product.id !== id)
  }

  resetCart(): void {
    this._products = []
  }

  get cartProducts(): ShoppingCartProduct[] {
    return this._products
  }

  constructor() { }
}
