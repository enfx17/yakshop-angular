import { TestBed } from '@angular/core/testing'

import {AuthGuard} from './auth.guard'

describe('GuestGuard', () => {
  let auth: AuthGuard

  beforeEach(() => {
    TestBed.configureTestingModule({})
    auth = TestBed.inject(AuthGuard)
  })

  it('should be created', () => {
    expect(auth).toBeTruthy()
  })
})
