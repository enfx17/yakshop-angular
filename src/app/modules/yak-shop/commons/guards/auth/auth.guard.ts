import { Injectable } from '@angular/core'
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, ActivatedRoute } from '@angular/router'
import {UserService} from '../../services/user/user.service'


@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(
        private userService: UserService
    ) {}

    canActivate(): boolean  {
        return this.userService.isLoggedIn()
    }

    canActivateChild(): boolean {
        return this.canActivate()
    }
}
