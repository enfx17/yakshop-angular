import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router'
import { Observable } from 'rxjs'
import {UserService} from '../../services/user/user.service'

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {

  constructor(private userService: UserService) {
  }

  canActivate(): boolean  {
    return !this.userService.isLoggedIn()
  }

  canActivateChild(): boolean {
    return this.canActivate()
  }
}
