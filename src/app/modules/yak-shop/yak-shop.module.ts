import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HomeComponent } from './pages/home/home.component'
import {YakShopRoutingModule} from './yak-shop-routing.module'
import { HerdComponent } from './pages/herd/herd.component'
import { MainMenuComponent } from './ui-components/main-menu/main-menu.component'
import { YakShopComponent } from './yak-shop.component'
import {HttpClientModule} from '@angular/common/http'
import { LabyakComponent } from './pages/herd/labyak/labyak.component'
import { LoginFormComponent } from './pages/home/login-form/login-form.component'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { UserWidgetComponent } from './ui-components/user-widget/user-widget.component'
import { DayPreviewComponent } from './ui-components/day-preview/day-preview.component'
import { ValidationMessageComponent } from './ui-components/validation-message/validation-message.component'
import {AuthGuard} from './commons/guards/auth/auth.guard'
import { OrderComponent } from './pages/order/order.component'
import { ProductsComponent } from './pages/products/products.component'
import { ProductComponent } from './pages/products/product/product.component'
import { NotFoundComponent } from './pages/not-found/not-found.component'


@NgModule({
  declarations: [HomeComponent, HerdComponent, MainMenuComponent, YakShopComponent, LabyakComponent, LoginFormComponent, UserWidgetComponent, DayPreviewComponent, ValidationMessageComponent, OrderComponent, ProductsComponent, ProductComponent, NotFoundComponent],
  imports: [
    YakShopRoutingModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    YakShopRoutingModule
  ],
  providers: [
    AuthGuard
  ]
})
export class YakShopModule { }
