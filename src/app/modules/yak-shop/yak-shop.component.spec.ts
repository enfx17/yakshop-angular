import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { YakShopComponent } from './yak-shop.component'

describe('YakShopComponent', () => {
  let component: YakShopComponent
  let fixture: ComponentFixture<YakShopComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YakShopComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(YakShopComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
