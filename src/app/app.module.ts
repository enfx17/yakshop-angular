import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppComponent } from './app.component'
import {YakShopModule} from './modules/yak-shop/yak-shop.module'
import {RouterModule, Routes} from '@angular/router'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import {YAK_SHOP_ROUTE_FRAGMENTS} from './modules/yak-shop/commons/constants/rotues'
import {NotFoundComponent} from './modules/yak-shop/pages/not-found/not-found.component'

const appRoutes: Routes = [
  {
    pathMatch: 'full',
    path: '',
    loadChildren: () => import('./modules/yak-shop/yak-shop.module').then(m => m.YakShopModule)
  },
  {path: YAK_SHOP_ROUTE_FRAGMENTS.notFound, component: NotFoundComponent},
  {path: '**', redirectTo: '/404'}
]


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    YakShopModule,
    RouterModule.forRoot(appRoutes),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
